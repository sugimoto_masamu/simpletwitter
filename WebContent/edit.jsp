<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>簡易Twitter</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="form-area">
			<div class="message-edit">
				<form action="./edit" method="post">
					<br /> <label for="message">編集内容を入力してください。</label>
					<textarea id="message" name="text" cols="100" rows="5"
						class="edit-tweet-box">${message.text}</textarea>
					<input type="hidden" name="editMessage" value="${message.id}">
					<br /> <input type="submit" value="更新">（140文字まで） <a href="./">戻る</a>
				</form>
			</div>
		</div>
		<div class="copyright">Copyright(c)SugimotoMasamu</div>
	</div>
</body>
</html>